
const LOOK = 'obserwacja';
const MOVE = 'ruch';
const PUSH = 'wypchnięcie';
const CONTROL = 'sterowanie';
const ACTIONS = [LOOK, MOVE, PUSH, CONTROL];

const SPECIAL_ABILITIES = {
  call: {
    name: 'wezwanie'
  },
  be_hidden: {
    name: 'niewidzialność'
  }
}



const NONE = 'NONE';
const SAFE = 'safe';
const OBSTACLE = 'obstacle';
const MORTAL_DANGER = 'mortal danger';
const DANGER_LEVELS = [NONE, SAFE, OBSTACLE, MORTAL_DANGER];


const ROOMS = {
  central_room: {
    name: 'POKÓJ CENTRALNY',
    danger_level: NONE
  },
  room_25: {
    name: 'POKÓJ 25',
    danger_level: NONE
  },
  key_room: {
    name: 'POKÓJ KONTROLNY',
    danger_level: NONE
  },
  empty_chamber: {
    name: 'PUSTE POMIESZCZENIE',
    danger_level: SAFE
  },
  vision_chamber: {
    name: 'SALA WIDEO',
    danger_level: SAFE
  },
  moving_chamber: {
    name: 'RUCHOMY POKÓJ',
    danger_level: SAFE
  },
  TUNNEL_chamber: {
    name: 'POKÓJ Z TUNELEM',
    danger_level: SAFE
  },
  control_chamber: {
    name: 'STEROWNIA',
    danger_level: SAFE
  },
  regeneration_room: {
    name: 'KOMORA REGENERACJI',
    danger_level: SAFE
  },
  robot_room: {
    name: 'KOMORA ROBOTÓW',
    danger_level: SAFE
  },

  vortex_room: {
    name: 'POKÓJ Z WIREM',
    danger_level: OBSTACLE
  },
  dark_chamber: {
    name: 'CIEMNY POKÓJ',
    danger_level: OBSTACLE
  },
  prison_cell: {
    name: 'CELA',
    danger_level: OBSTACLE
  },  
  pivoting_room: {
    name: 'KOMORA OBROTOWA',
    danger_level: OBSTACLE
  },  
  cold_chamber: {
    name: 'KOMORA CHŁODU',
    danger_level: OBSTACLE
  },  
  mac_room: {
    name: 'POKÓJ Z.R.B.',
    danger_level: OBSTACLE
  },  
  mirror_room: {
    name: 'POKÓJ LUSTER',
    danger_level: OBSTACLE
  },  
  jamming_room: {
    name: 'KOMORA ZAKŁÓCANIA',
    danger_level: OBSTACLE
  },  

  acid_bath: {
    name: 'WANNA Z KWASEM',
    danger_level: MORTAL_DANGER
  },
  mortal_chamber: {
    name: 'KOMORA ŚMIERCI',
    danger_level: MORTAL_DANGER
  },
  trapped_chamber: {
    name: 'POKÓJ PUŁAPKA',
    danger_level: MORTAL_DANGER
  },
  shredder_room: {
    name: 'KOMORA PIŁ',
    danger_level: MORTAL_DANGER
  },
  flooded_chamber: {
    name: 'ZALANA KOMORA',
    danger_level: MORTAL_DANGER
  },

  paranoia_room: {
    name: 'POKÓJ PARANOI',
    danger_level: MORTAL_DANGER
  },
  illusion_chamber: {
    name: 'POKÓJ ILUZJI',
    danger_level: MORTAL_DANGER
  },
  timer_room: {
    name: 'POKÓJ ZEGAROWY',
    danger_level: MORTAL_DANGER
  },
}

const MODE_COOP_EASY = 'KOOPERACJA: PIERWSZA PRÓBA';
const MODE_COOP_EASY_ROOMS = [
  ROOMS.mortal_chamber,
  ROOMS.trapped_chamber,
  ROOMS.trapped_chamber,
  ROOMS.flooded_chamber,
  ROOMS.flooded_chamber,
  ROOMS.shredder_room,
  ROOMS.shredder_room,
  ROOMS.acid_bath,
  ROOMS.acid_bath,
  ROOMS.prison_cell,
  ROOMS.prison_cell,
  ROOMS.pivoting_room,
  ROOMS.pivoting_room,
  ROOMS.vortex_room,
  ROOMS.cold_chamber,
  ROOMS.cold_chamber,
  ROOMS.dark_chamber,
  ROOMS.dark_chamber,
  ROOMS.vision_chamber,
  ROOMS.empty_chamber,
  ROOMS.empty_chamber,
  ROOMS.control_chamber,
  ROOMS.room_25,
  ROOMS.key_room,
]

const GAMEMODE_ROOMS = {
  [MODE_COOP_EASY]: MODE_COOP_EASY_ROOMS,
}

// MODE_COOP_EASY_ROOMS.forEach(room => {
//   console.log(room.name);
// })
// console.log(MODE_COOP_EASY_ROOMS.length);

const EXIT_ZONES = [0, 1, 3, 4, 5, 9, 15, 19, 20, 21, 23, 24];
const MIDDLE_ZONES = [2, 6, 7, 8, 10, 11, 13, 14, 16, 17, 18, 22];
const ROOMS_ONLY_IN_EXIT_ZONES = [
  ROOMS.room_25,
  ROOMS.key_room,
  ROOMS.vision_chamber,
  ROOMS.moving_chamber,
  ROOMS.robot_room
];

const PARAMS_BY_PLAYER_COUNT = {
  1: {chars: 4, rounds: 8},
  2: {chars: 2, rounds: 8},
  3: {chars: 2, rounds: 6},
  4: {chars: 1, rounds: 8},
  5: {chars: 1, rounds: 7},
  6: {chars: 1, rounds: 6}
}

const AVAILABLE_CHARACTERS = {
  jennifer: {
    name: 'Jennifer',

  }
}


class Room25 {
  constructor() {
    this.players = [];
    this.rooms = new Array(25).fill(null);
    this.game_mode = null;
    this.characters_per_player = 1;
    this.rounds = 8;
  }

  setGameMode(game_mode) {
    if (!(game_mode in GAMEMODE_ROOMS)) return;
    this.game_mode = game_mode;
    
    this.rooms[12] = ROOMS.central_room;

    let rooms_left = GAMEMODE_ROOMS[game_mode];
    let for_exit_zones = putAside(rooms_left, ROOMS_ONLY_IN_EXIT_ZONES);
    MIDDLE_ZONES.forEach(idx => {
      this.rooms[idx] = getRandomRoom(rooms_left);
    })

    addRemaining(rooms_left, for_exit_zones);
    EXIT_ZONES.forEach(idx => {
      this.rooms[idx] = getRandomRoom(rooms_left);
    })

    console.log(this.rooms);

    function putAside(rooms, to_put) {
      let aside = [];
      let new_rooms = [];
      rooms.forEach((room, index) => {
        if (to_put.indexOf(room) >= 0) {
          aside.push(room);
        } else {
          new_rooms.push(room);
        }
      })
      rooms.length = 0;
      rooms.push(...new_rooms);
      return aside;
    }

    function addRemaining(rooms, to_add) {
      rooms.push(...to_add);
    }

    function getRandomRoom(rooms) {
      const random_room_idx = Math.floor(Math.random() * rooms.length);
      const room = rooms[random_room_idx];
      rooms.splice(random_room_idx, 1);
      return room;
    }
  }

  addPlayer(name) {
    const player = new Player(name);
    this.players.push(player);
  }

  startGame() {
    // kazdy gracz wybiera postacie
    this.playersChooseCharacters();
    // kazdy gracz wybiera sasiednie pole do podejrzenia
      // możliwość wypowiedzenia koloru pokoju przy podglądaniu


  }

  playersChooseCharacters() {
    const params = PARAMS_BY_PLAYER_COUNT[this.players.length];
    this.characters_per_player = params.chars;
    this.rounds = params.rounds;
    console.log(this);

    this.players.forEach(player => {

    })
  }
}

class Player {
  constructor(name) {
    this.name = name;
    this.clue = null;
  }
}


const room25 = new Room25();
room25.setGameMode(MODE_COOP_EASY);
room25.addPlayer('Arthan');
//room25.addPlayer('Nahtra');

room25.startGame();
